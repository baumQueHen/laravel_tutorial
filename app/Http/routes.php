<?php
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Input;
/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/
class Paging extends Illuminate\Pagination\LengthAwarePaginator{
    /**
     * Get a URL for a given page number.
     *
     * @param  int  $page
     * @return string
     */
    public function url($page)
    {
        if ($page <= 0) {
            $page = 1;
        }
        // If we have any extra query string key / value pairs that need to be added
        // onto the URL, we will put them in query string form and then attach it
        // to the URL. This allows for extra information like sortings storage.
        $parameters = [$this->pageName => $page*10+1];
        if (count($this->query) > 0) {
            $parameters = array_merge($this->query, $parameters);
        }
        return $this->path
            .(Str::contains($this->path, '?') ? '&' : '?')
            .http_build_query($parameters, '', '&')
            .$this->buildFragment();
    }

}

Route::get('/', function () {
    return view('welcome');
});

Route::get('/page/', function(Illuminate\Http\Request $request){
    dump($request->input('start'));
       $paginator = new Paging(
            range(1,500),
            //[],
            511, // total
            10, // perPage
            ($request->input('start')-1)/10,
            ['path' => '',
            'pageName' => 'start']
        );
    $paginator->fragment('answer');

dump($paginator->toArray());
        return $paginator->render();
});

